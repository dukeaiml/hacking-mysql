# Hacking MySQL

## Setup 
Setup Database
```mysql
ALTER USER 'root'@'localhost' IDENTIFIED BY 'dbpassword';
CREATE DATABASE mydemo;
USE mydemo;
```
Create Table and Insert Data
```mysql
CREATE TABLE users(
                      id INT AUTO_INCREMENT PRIMARY KEY,
                      name VARCHAR(50),
                      email VARCHAR(50)
);
INSERT INTO users (name, email)
VALUES ('John', 'john@gmail.com');

INSERT INTO users (name, email)
VALUES ('Jane', 'jane@yahoo.com');
```

Query Data
```mysql
SELECT * FROM users;
```

## Extend

Extending a directors_notes Table.

```mysql
CREATE TABLE directors_notes (
note_id INT AUTO_INCREMENT PRIMARY KEY,
film_id INT,
director_note VARCHAR(255),
edit_date DATE
);
```

Insert
```mysql

INSERT INTO directors_notes (film_id, director_note, edit_date)
VALUES (1, 'Fix sound in minute 20', '2023-02-01');

INSERT INTO directors_notes (film_id, director_note, edit_date)
VALUES (2, 'Shorten battle scene', '2023-02-03');
```

Select
```mysql
SELECT f.title, n.director_note, n.edit_date
FROM film f
LEFT JOIN directors_notes n
ON n.film_id = f.film_id;

DROP TABLE directors_notes;
```

## Export to Create Python Web Service

```mysql
SELECT * FROM sakila.actor 
INTO OUTFILE '/tmp/actors.csv' 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
```

```python
import csv 
from http.server import HTTPServer, BaseHTTPRequestHandler

class HTTPHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        with open('/tmp/actors.csv') as f:
            reader = csv.reader(f)
            self.wfile.write(b"\n".join([','.join(row).encode() for row in reader]))

httpd = HTTPServer(('localhost', 8081), HTTPHandler)
httpd.serve_forever()
```